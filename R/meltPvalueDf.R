#' meltPvalueDf
#'
#'takes a pvalue.dataframe returned by the 'calculate.pvalues()' function and reshapes to have the following columns:
#'Rank, seed, pval
#'This function is used to convert the pvalues dataframe in a format that can be easily annotated with additional columns (i.e. miRNA names) and can be easily plotted with ggplot2
#' @param pval.df
#'
#' @return a dataframe in tall format for plotting.
#' @export

meltPvalueDf <- function(pval.df){
  Rank=1:length(rownames(pval.df))
  pval.df <- cbind(Rank, pval.df)
  pval.df <- gather(pval.df, seed, pval, -1)
  return (pval.df)
}


