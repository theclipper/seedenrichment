#' @title targets.matrix.from.targetscan
#'
#' @description This function converts a summary count dataframe downloaded by targetscan to
#' a matrix of in which each row is a gene symbol, each columns is a miRNA seed, and each cell is the number of seed matches.
#' The targetscan dataframe must have the following columns:
#' "Gene.Symbol" => Column containing gene symbols
#' "miRNA.family" => this is the seed sequence
#' "Total.num.conserved.sites"
#' "Total.num.nonconserved.sites"
#' "Number.of.conserved.8mer.sites"
#' "Number.of.nonconserved.8mer.sites"
#'
#' @param targetscan A data.frame
#' @param seeds A list of seeds to use as columns in the matrix
#' @param genes A list of gene Symbols to use as rows in the matrix
#' @param conserved.only Logical. Use only conserved sites? Default = T
#' @param eightmers.only Logical. Use only eightmer sites? Default = F
#' @return
#' returns a matrix with genes as rows and miRNA as columns
#' @export


targets.matrix.from.targetscan <- function(targetscan, seeds=character(), genes=character(),
                                           conserved.only = T,
                                           eightmers.only = F) {
  if (length(seeds)==0) {
    seeds = unique(targetscan$miRNA.family)
  }

  if (length(genes)==0) {
    genes = unique(targetscan$Gene.Symbol)
  }
  #extract desired sites and simplify data.frame
  print("Formatting targetscan table")
  #first simplify targetscan by removing genes not appearing in the genes list
  print("filtering by genes")
  targetscan <- filter(targetscan, Gene.Symbol %in% genes)
  #filtering by seeds
  print("filtering by seeds")
  targetscan <- filter(targetscan, miRNA.family %in% seeds)

  if (conserved.only == T && eightmers.only == F){
    targetscan <- targetscan[which(targetscan$Total.num.conserved.sites > 0), ]
    total.sites <- targetscan$Total.num.conserved.sites
  } else if (conserved.only == T && eightmers.only == T){
    targetscan <- targetscan[which(targetscan$Number.of.conserved.8mer.sites > 0), ]
    total.sites <-  targetscan$Number.of.conserved.8mer.sites
  } else if (conserved.only == F && eightmers.only == T) {
    targetscan <- targetscan[which(targetscan$Number.of.conserved.8mer.sites + targetscan$Number.of.nonconserved.8mer.sites > 0), ]
    total.sites <-  targetscan$Number.of.conserved.8mer.sites + targetscan$Number.of.nonconserved.8mer.sites
  } else if (conserved.only == F && eightmers.only == F) {
    targetscan <- targetscan[which(Total.num.conserved.sites + Total.num.nonconserved.sites > 0), ]
    total.sites <- targetscan$Total.num.conserved.sites + targetscan$Total.num.nonconserved.sites
  }
  targetscan <-  cbind(targetscan[,c("Transcript.ID","Gene.Symbol","miRNA.family" ,"Representative.miRNA")], total.sites) #simplifies dataframe
  print("Done!")
  #generates targets matrix
  print("Generating targets matrix skeleton")

  result.matrix <- matrix(nrow=length(genes), ncol = length(seeds), data = rep(0, length(seeds)*length(genes)))
  colnames(result.matrix) <- seeds
  rownames(result.matrix) <- genes
  print("Filling targets matrix. This may take several minutes, be patient!")
  return(.fill.targets.matrix(result.matrix, genes, seeds, targetscan))
}


.fill.targets.matrix <- function(targets.matrix, genes, seeds, targetscan) {
  n=length(genes)
  i=0
  pb <- txtProgressBar(min = 0, max = n, style = 3)
  for (gene in genes){
    i=i+1
    setTxtProgressBar(pb, i)
    if (!(gene %in% targetscan$Gene.Symbol)) {
      #do nothing leaves 0s for all miRNAs
    } else {
      gene.targets=subset(targetscan, Gene.Symbol == gene)
      for (seed in seeds) {
        if (seed %in% gene.targets$miRNA.family) {
          targets.matrix[gene, seed]<-(sum(gene.targets[which(seed %in% gene.targets$miRNA.family), "total.sites"])>0)*1
        } else {
          targets.matrix[gene, seed]<-0
        }
      }
    }
  }
  close(pb)
  return (targets.matrix)
}


